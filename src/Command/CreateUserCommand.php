<?php

namespace App\Command;

use App\Entity\User;
use App\Enum\DefaultRegionEnum;
use App\Repository\UserRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;
use Symfony\Component\PasswordHasher\Hasher\UserPasswordHasherInterface;

class CreateUserCommand extends Command
{
    protected static $defaultName = 'app:create-user';
    private $userRepository;
    private $entityManager;
    private $passwordHasher;

    public function __construct(
        UserRepository $userRepository,
        EntityManagerInterface $entityManager,
        UserPasswordHasherInterface $passwordHasher
    ) {
        parent::__construct();
        $this->userRepository = $userRepository;
        $this->entityManager = $entityManager;
        $this->passwordHasher = $passwordHasher;
    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $io = new SymfonyStyle($input, $output);

        $user = new User();
        $user
            ->setEmail('user@user.com')
            ->setPassword($this->passwordHasher->hashPassword($user,'passpass'))
            ->setName('Super')
            ->setSurName('User')
            ->setRoles(['ROLE_USER'])
            ->setActive(true);

        $this->entityManager->persist($user);
        $this->entityManager->flush();

        $io->success('User has been created');

        return Command::SUCCESS;
    }
}